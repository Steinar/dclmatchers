import pytest
from collections import OrderedDict
from src.dclmatchers import ListDclMatcher, AmbiguousKeyTable

def test_normal_matching():
    assert 1 == 1

def create_simple_mapping():
    return ListDclMatcher(dict({"north": 1, "south": 2, "east": 3, "west": 4}))

def test_membership_in_empty_map():
    m = ListDclMatcher()
    with pytest.raises(KeyError):
        m["abc"]
    assert False == ("abc" in m)

def test_get_substring():
    m = create_simple_mapping()
    assert 1 == m['n']
    assert 2 == m['so']
    assert 3 == m['eas']
    assert 4 == m['west']
    with pytest.raises(KeyError):
        m["west1"]

def test_set_substring():
    m = create_simple_mapping()
    m['n'] = 5
    assert 5 == m['north']
    m['east'] = 6
    assert 6 == m['e']

def test_set_superstring():
    m = create_simple_mapping()
    with pytest.raises(AmbiguousKeyTable):
        m['northish'] = 1.1

def test_initialize_superstring():
    m = OrderedDict()
    m['n'] = 1
    m['nn'] = 2
    with pytest.raises(AmbiguousKeyTable):
        ListDclMatcher(m)

def test_initialize_substring():
    m = OrderedDict()
    m['nn'] = 1
    m['n'] = 2
    with pytest.raises(AmbiguousKeyTable):
        ListDclMatcher(m)

def test_iteration():
    m = create_simple_mapping()
    itemsum = 0
    for key, value in m.items():
        itemsum += value
    assert itemsum == 10
    keyset = set()
    for key in m:
        keyset.add(key)
    assert set({"north", "south", "east", "west"}) == keyset

def test_contains():
    m = create_simple_mapping()
    assert True == ('no' in m)
    assert False == ('yes' in m)
    assert False == ('northwards' in m)
    assert False == ('' in m)

def test_deletion():
    m = create_simple_mapping()
    with pytest.raises(KeyError):
        del m['abc']
    del m['n']
    del m['sou']
    del m['west']
    assert 1 == len(m)
    del m['e']
    assert 0 == len(m)
    with pytest.raises(KeyError):
        del m['e']

def test_length():
    m = ListDclMatcher()
    assert 0 == len(m)
    m['abc'] = 'ABC'
    assert 1 == len(m)
    m['def'] = 'DEF'
    assert 2 == len(m)
    del m['d']
    assert 1 == len(m)

def test_ensure_only_strings_are_keys():
    m = ListDclMatcher()
    with pytest.raises(TypeError):
        m[1] = 2

def test_read_full_keys():
    m_empty = ListDclMatcher()
    nothing = m_empty.full_key('n')
    assert None == nothing
    m = create_simple_mapping()
    full_name = m.full_key('n')
    assert full_name == 'north'
    full_name_for_full_name = m.full_key('north')
    assert full_name_for_full_name == 'north'
    almost_match = m.full_key('norble')
    assert None == almost_match

def test_insertion_check():
    m = create_simple_mapping()
    assert False == m.may_insert('')
    assert False == m.may_insert('north')
    assert False == m.may_insert('nor')
    assert True == m.may_insert('wesz')
    assert False == m.may_insert('westle')
    m['wesz'] = 5
    assert False == m.may_insert('wes')

def test_string_rendering():
    m = create_simple_mapping()
    assert ("ListDclMatcher[('east', 3), ('north', 1), ('south', 2),"
            + " ('west', 4)]" == repr(m))

def test_key_shortening():
    m = create_simple_mapping()
    assert 1 == m.shortest_key('nor')
    assert 1 == m.shortest_key('north')
    assert -1 == m.shortest_key('')
    assert -1 == m.shortest_key('zzz')
    assert 1 == m.shortest_key('w')
    assert -1 == m.shortest_key('a')
    assert 1 == m.shortest_key('ea')
    m['norbl'] = 5
    assert 4 == m.shortest_key('norb')
