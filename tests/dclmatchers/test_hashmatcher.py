import pytest
from src.dclmatchers import build_dictionary, will_break_key_uniqueness

def test_smoke():
    m = build_dictionary({'aaa': 1, 'aab': 2, 'abc': 3})
    assert 4 == len(m)

def test_ambiguous_key_table():
    with pytest.raises(ValueError):
        build_dictionary({'aaa': 1, 'aaab': 2, 'abc': 3})

def test_no_empty_keys():
    with pytest.raises(ValueError):
        build_dictionary({'a': 1, 'b': 2, '': 3})

def test_only_string_keys():
    with pytest.raises(TypeError):
        build_dictionary({'a': 1, 'b': 2, 4: 3})

def test_key_check():
    keys = ['aaa', 'aab']
    assert True == will_break_key_uniqueness('aa', keys)
    assert True == will_break_key_uniqueness('aaa', keys)
    assert False == will_break_key_uniqueness('aax', keys)
    assert False == will_break_key_uniqueness('b', keys)
    assert True == will_break_key_uniqueness('aaaa', keys)
