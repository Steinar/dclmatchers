                                  dclmatchers

Mapping types and tools to match strings by shortest unique prefix. Named after
DCL since that was my first exposure to using the pattern heavily in a UI.


    Examples

This module has two key entry points, the class ListDclMatcher and the factory
function build_dictionary(). ListDclMatcher is a mutable mapping type containing
string keys where any lookup using a unique prefix of a key gives a successful
return of the value. build_dictionary() takes a dict() with string keys, and
then return a new dicts with all possible unique key prefixes derived from the
initial dictionary. It is all very much easier to show than tell:

    >>> import dclmatchers
    >>> a = dclmatchers.ListDclMatcher()
    >>> a["python"] = 1
    >>> a['p']
    1
    >>> a["pl/1"] = 2
    >>> a["prolog"] = 3
    >>> a['p']
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
      File "/home/sk/sentral/pagure/dclmatchers/src/dclmatchers/__init__.py", line 57, in __getitem__
	raise KeyError(key)
    KeyError: 'p'
    >>> a['py']
    1
    >>> a['pyt']
    1

In other words, it's the common “write as much that it's unambiguous which prefix
is referred”. Conversely, using build_dictionary() to create a static table:

    >>> a = dclmatchers.build_dictionary({"python": 1, "pl/1": 2, "prolog": 3})
    >>> a
    {'py': 1, 'pyt': 1, 'pyth': 1, 'pytho': 1, 'python': 1, 'pl': 2, 'pl/': 2, 'pl/1': 2, 'pr': 3, 'pro': 3, 'prol': 3, 'prolo': 3, 'prolog': 3}


    Licence

Copyright 2020-2021 Steinar Knutsen

Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the
European Commission – subsequent versions of the EUPL (the “Licence”); You may
not use this work except in compliance with the Licence. You may obtain a copy
of the Licence at:

https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12

Unless required by applicable law or agreed to in writing, software distributed
under the Licence is distributed on an “AS IS” basis, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
specific language governing permissions and limitations under the Licence.
