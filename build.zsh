#! /bin/zsh

python -m venv ./.build-env
source ./.build-env/bin/activate
pip install --upgrade build
pip install -r requirements.txt
make all
